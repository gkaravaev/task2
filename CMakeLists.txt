cmake_minimum_required(VERSION 3.1.0)
project(tritset)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp tritset.cpp tritset.h)
add_executable(tritset main.cpp tritset.cpp tritset.h)
add_executable(unittest test.cpp tritset.cpp)