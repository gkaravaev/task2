//
// Created by Gregpack on 30-Sep-18.
//

#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "tritset.h"
#include <sstream>

TEST_CASE ("Constructor") {
    Tritset set(1000);
    size_t allocLength = set.capacity();
    REQUIRE(allocLength >= 1000 * 2 / 8 / sizeof(uint));
}

TEST_CASE ("Overloaded operators") {
    Tritset set(20);
    set[10] = Trit::True;
    set[5] = Trit::False;
    set[2] = Trit::Unknown;
    REQUIRE(set[10] == Trit::True);
    REQUIRE(set[5] == Trit::False);
    REQUIRE(set[2] == Trit::Unknown);
    set[10] = Trit::False;
    set[5] = Trit::Unknown;
    set[2] = Trit::True;
    REQUIRE(set[2] == Trit::True);
    REQUIRE(set[10] == Trit::False);
    REQUIRE(set[5] == Trit::Unknown);
}

TEST_CASE("Memory allocation") {
    Tritset set(1000);
    size_t allocLength = set.capacity();

    set[10000000] = Trit::Unknown;
    REQUIRE(allocLength == set.capacity());

    if (set[20000000] == Trit::True) {}
    REQUIRE(allocLength == set.capacity());

    set[1000000] = Trit::True;
    REQUIRE(allocLength < set.capacity());

    allocLength = set.capacity();
    set[10000000] = Trit::Unknown;
    set[100000] = Trit::False;
    REQUIRE(allocLength == set.capacity());
}

TEST_CASE ("Shrinking") {
    Tritset set(1000);
    size_t allocLength = set.capacity();
    set.shrink();
    REQUIRE(allocLength > set.capacity());
}

TEST_CASE ("Cardinality") {
    Tritset setA(33);
    for (int i = 0; i < 33; i++) {
        if (i % 3 == 0)
            setA[i] = Trit::True;
        else if (i % 3 == 1)
            setA[i] = Trit::False;
        else
            setA[i] = Trit::Unknown;
    }
    REQUIRE(setA.cardinality(Trit::True) == 11);
    REQUIRE(setA.cardinality(Trit::False) == 11);
    REQUIRE(setA.cardinality(Trit::Unknown) == 10);

}

TEST_CASE ("Initializer list") {
    Tritset ts{Trit::True, Trit::True, Trit::False, Trit::Unknown};
    REQUIRE(ts[0] == Trit::True);
    REQUIRE(ts[1] == Trit::True);
    REQUIRE(ts[2] == Trit::False);
    REQUIRE(ts[3] == Trit::Unknown);
    REQUIRE(ts.length() == 3);
}

TEST_CASE ("Single trit") {
    Tritset ts(1000);
    ts[0] = Trit::True;
    Trit t = ts[0];
    REQUIRE(t == ts[0]);
}

TEST_CASE ("Output") {
    Tritset set{Trit::True, Trit::False, Trit::Unknown, Trit::True};
    std::stringstream f;
    f << set << std::endl;
    REQUIRE(f.str() == "10?1\n");
}

TEST_CASE ("Range-based for loop") {
    Tritset set (10);
    for (uint i = 0; i < 10; i++) {
        set[i] = Trit::True;
    }
    uint check = 0;
    for (auto a : set) {
        if (a == Trit::True) check++;
    }
    REQUIRE(check == 10);
}

TEST_CASE ("Logical operations") {
    Tritset setA(10);
    for (int i = 0; i < 10; i++)
        setA[i] = Trit::True;
    Tritset setB(20);
    for (int i = 0; i < 20; i++)
        setB[i] = Trit::False;
    Tritset setC = setA & setB;
    Tritset setD = setA | setB;
    Tritset setE = !setA;
    REQUIRE(setC.capacity() == setB.capacity());
    REQUIRE(setD.capacity() == setB.capacity());
    REQUIRE(setE.capacity() == setA.capacity());
    REQUIRE(setC.cardinality(Trit::False) == 20);
    REQUIRE(setD.cardinality(Trit::True) == 10);
    REQUIRE(setE.cardinality(Trit::False) == 10);
};

TEST_CASE ("Trimming and length") {
    Tritset setA(50);
    for (int i = 0; i < 32; i++)
        setA[i] = Trit::True;
    REQUIRE(setA.length() == 32);
    setA.trim(25);
    REQUIRE(setA.length() == 25);

}

